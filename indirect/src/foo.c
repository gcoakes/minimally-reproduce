#include <stdio.h>
void __attribute__((constructor)) construct();
void __attribute__((destructor)) destruct();
void construct()
{
    printf("Constructor.\n");
}
void destruct()
{
    printf("Destructor.\n");
}