fn main() {
    cc::Build::new()
        .file("src/foo.c")
        .cargo_metadata(false)
        .static_flag(true)
        .compile("foo");
    println!(
        "cargo:rustc-link-search=native={}",
        std::env::var("OUT_DIR").unwrap()
    );
    println!("cargo:rustc-link-lib=static:+whole-archive=foo");
}
